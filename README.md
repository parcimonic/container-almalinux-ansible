# Alma Linux container

This is an [AlmaLinux](https://almalinux.org/) based image that I'm using for my
[Molecule](https://molecule.readthedocs.io/en/latest/index.html) tests.

URL: `registry.gitlab.com/parcimonic/container-almalinux-ansible:latest`
