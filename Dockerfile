# hadolint ignore=DL3007
FROM almalinux:latest

# Add sudo so we can `become_user` in Ansible.
# hadolint ignore=DL3041
RUN set -euf && \
    dnf install -y \
    python \
    sudo && \
    dnf clean all

# Add a non-root user for some Ansible tasks that
# target unprivileged users.
RUN useradd -m milkyway
